# Learn To Code

### Simple lessons with simple examples, entry level programming using javascript

## Installation

### 1. Prerequesites

We're going to use a couple of tools for this

* git
* visual studio code
* node js

1. Start by installing git. you can either use an atlassian tool or any other you prefer
2. Install visual studio code from Microsoft
3. Install node js (and if you need to install separately npm (node package manager))

### 2. setup project

Grab the code from this page (https://bitbucket.org/leafblight/learntocode.git) through your git tool of choice
put it in an easily accessible place

Open Visual studio code and then choose "open folder" from the menu
Install Code runner extension from the extension manager and reload the session
start by checking out the git branch "beginner" and then you will be good to go, up in the right corner of visual studio code you will have a play button which you can run your code with