/**
 * Exempelfil - exempel på kod
 */

//detta är en kommentar. en kommentar är text i ett kodstycke som inte körs, så att man kan dokumentera. allt som behövs är två slashes

/*
    man kan även göra en flerradig kommentar via att lägga två asterisker emellan slashen
    tada!

    Nedan kommer nu följa ett antal olika förklaringar på kodgrejer

*/

/**
 * 1. funktioner
 * 
 * en funktion är ett set med kommandon som återupprepat kan kallas så många gånger man vill.
 */

 function hello() {
     console.log("hello, world!");
 }

//du kallar sedan på en funktion så här
hello();

//du kan t.om. kalla på funktioner i funktioner

function doubleHello() {
    hello();
    hello();
}

//funktioner kan kaN även ta emot information i form av "argument"
function helloWithArgument(arg1, arg2) {
    console.log( arg1 + " hello, World!!! " + arg2);
}

helloWithArgument("EHHHHH, ", " WOHOO!");
//i javascript finns det inte bestämt vad det är för värden man utnyttjar
helloWithArgument(1,2);


//funktioner kan även returnera information
function getSomething() {
    return "this is a returned string";
}

console.log(getSomething());

/**
 * Summering
 */

 //nyckelord som deklarerar en funktion      funktionsnamn        argument till funktionen
 function                                    someFunctionName     (arg1, arg2) {
    //funktionskropp. innehåller exempelvis returvärde
    return arg1 + arg2;
 }

 //alternativa sätt att deklarera funktioner på:
 //functionName = function() { ... }
 //functionName = () => { ... }


/**
 * 2. Variabler
 * 
 * variabler är namngivna pekare på saker. i princip allt du gör i programmering startar med variabler
 */

 // en variabel deklareras så här:
 var variable;
 // variabelns värde sätts så här (Definition av en variabel)
 variable = "hello, world";
 // du kan sen använda variabeln:
 console.log(variable);
// du kan omdefinera en variabel hur många gånger du än vill (i javascript behöver den inte ens behålla ett och samma värdetyp)
variable = "new string";
variable = 23;
variable = function () {
}
// du kan även ha deklaration och definition på en och samma rad:
var var2 = "declare and define!";

/**
 * 3. Typer av värden
 */

//detta är vad som kallas en sträng (string), dvs en "sträng" med bokstäver
var str = "lorem ipsum";

//detta är en heltalssiffra, s.k. integer eller int
var int = 1234;

//detta är en flyttalssiffra (floating point), eller en decimalformatssiffra
var float = 12.4;

//detta är en "boolean", en flagga som kan vara satt till "sann" eller "falsk"
var boolean = true;

//om man adderar två värden med varandra så får man tillbaka det minst "avancerade värdet" som går att få
console.log(str + int); //resulterar i strängen "lorem ipsum1234", samma resultat hade det blivit för float
console.log(float + int); //resulterar i 1246.4, ett flyttal
console.log(int + int); //kör man samma typ så behålls typen (såvida det inte är dividerar)
console.log(int / 3); //som exempel
console.log(boolean + str);
console.log(boolean + int);  // specialfall: den boolska variablen ger ingen förändring på output här

