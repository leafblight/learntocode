const assert = require('assert');
/**
 * Uppgifter
 * 
 * detta kommer vara test på om man har förstått exempel nr 1
 */

// 1. se till att funktionen returnerar "hello, world"
function test1() {
    return "err";
}

// 2. gör så att funtionen tar emot två argument och sedan returnerar argumenten adderade
function test2() {
    return ;
}

// 3. skapa en funktion test3 som returnerar siffran 7.5

// 4. funktionen nedanför är syntaktiskt fel, rätta till den
// tips: när du kör filen kommer felmeddelanden dyka upp i konsolen (samma plats som console.log meddelanden skrivs ut)
// function test4 = () {
//     return true;
// }


/**
 * extrauppgifter
 * 
 * Dessa behövs inte göras annat än för att lära sig lite mer grejer om man känner för det
 */

 // 1. gör så att nedanstående funktion alltid skriver ut argumenten som en sträng
 // hur mycket förändring på funktionen behövde du göra? skulle du kunna minska ner det med nåt du har lärt dig hitintills?

 function extra1(arg1, arg2) {
    console.log(arg1 + arg2);
 }

 extra1(12.5, 200);


// 2. skapa två funktioner som returnerar varsin textsträng. kalla på dessa och spara resultaten till en gemensam variabel och skriv ut den med console.log
// hur många kommandon behövde du göra för att få ut strängen? kan du skippa några kommandon kanske?

//testning, ignorera nedanför

assert(test1() === "hello, world", "Test 1 returns \"hello, world\"")
assert(test2(2,2.4) === 4.4, "Test 2 returns addition between 2 and 2.4");
assert(test2("hey there, ",2.4) === "hey there, 2.4", "Test 2 returns addition between \"hey there, \" and 2.4");
assert(test3() === 7.5, "Test 3 returns 7.5");
assert(test4(), "Test 4 works and returns true");