/**
 * 1. if satser
 */

 //detta är en if sats
 if(true) {
     console.log("it works!");
 }
/**
 * en if sats tar emot en rad kod som ska tolkas som sant eller falskt
 * detta kan vara ett värde eller en jämförelse
 * 
 * i de flesta programmeringspråk jämför man med hjälp av ==
 * i javascript finns även ===, vilket man föredrar att använda då den även ser till att det är samma datatyp 
 * 
 * dvs 1.0 == 1 är sant
 * medan 1.0 === 1 är falskt, då de inte är samma värdetyp
 * 
 */

 //här säger vi att 1 är lika med 1, vilket är sant
 if(1 === 1) {
     console.log("this will run");
 }

 //men om vi jämför 1 med två ser vi att de skiljer sig
 if(1 === 2) {
     console.log("this not will run!");
 }

 //man kan även använda if för att se om ett värde är satt
 var someVal;

//detta körs inte eftersom someVal inte är definerad än
//det hade heller inte kört om someVal var satt till false, undefined, tom sträng eller 0 då 0 ses som falskt i en if sats
if(someVal) {
    console.log("this won't run");
}

someVal = "it is time";
//nu kommer den köra
if(someVal) {
    console.log("this will run");
}

/**
 * En if sats kan även ta emot en kedja av påståenden med nyckelorden || och && 
 * 
 * || betyder och/eller, dvs att antingen påståendet till vänster eller höger om den måste vara sant
 * && betyder och, dvs att båda påståenden runt den måste vara sanna 
 */

//denna kommer inte köra då ett av påståendena är falska
if(1 === 1 && 1 === 2) {
    console.log("won't run");
}
//hade vi istället använt || så hade vi fått igenom det då i alla fall ett påstående är sant
if(1 === 1 || 1 === 2) {
    console.log("I ran!");
}

//man kan ha hur långa kedjor man än vill
if(1 === 1 || 1 === 2 && 2 === 2 || 1 === 2) {
    console.log("that's a long chain");
}

//man kan även kräva att ett värde ska vara falskt, genom att lägga ! framför
var falseVal = false;
if(!falseVal) {
    console.log("it runs even though it's a falsy value!!!");
}
// "!" vänder på värdet, man kan se det som "inte", så i detta fallet blir det "inte sant", vilket är sant då värdet är falskt

//intressant nog kan man utnyttja detta värdet också på booleska variabler
var aValue = false;
aValue = !aValue;
//vi flippade aValue precis, den är nu mera true
if(aValue) {
    console.log("yes!!!");
}

//! kan också användas vid jämförelse, m.h.a. detta får man jämförelsenycklarna != och !==
if(1 !== 2) {
    console.log("true, 1 is not equal to 2");
}

//till if satser kan man lägga till nyckelorder "else"

if(1 === 2) {
    console.log("won't run");
} else {
    console.log("will run, since previous statement didn't go through");
}

//else säger att om ifen inte gick igenom, gör detta istället
//man kan även använda if i kombination med else så att man får olika kodstycken som körs i prioriteringsordning av vad som först stämmer

if(1 !== 1) {
    console.log("won't run");
} else if(1 === 1) {
    console.log("will run!");
} else {
    console.log("won't run since the previous statement went through!");
}

/**
 * 2. switch satser (a.k.a switch case satser)
 * 
 * switch satser har ett liknande användningsområde som if satser, fast mer specifikt
 * en switch sats tar emot ett värde och ger sedan ett antal olika sekvenser kod som ska köras i kaskad efter vad värdet är
 */

var anInteger = 20;

switch(anInteger) {
case(10):
    console.log("the value is 10!");
    break;
case(20):
    console.log("the value is 20!");
    break;
default:
    console.log("nope..");
}

/**
 * för varje värde man vill jämföra med så gör man ett "case"
 * 
 * efter case förklaringen följer kodsekvensen
 * varning: för att avbryta swithcen från att köra all kod så använder man sig av nyckelordet break. om man inte kör break fortsätter den att köra koden i kaskad
 * 
 * default är vad som ska köras om inget annat gick igenom, den behöver inte vara satt
 */

//nedan följer ett exempel på kaskadande switch satser
var someStr = "this is a string";
switch(someStr) {
case(1):
    console.log("this only runs when the value is 1");
case("this is a string"):
    console.log("this is only run the value is \"this is a string\" or 1");
case("some other string"):
    console.log("this is run if the value is \"this is a string\" or \"some other string\" or 1");
default:
    console.log("this is always run since we ain't using break");
}

//hade vi istället haft break i slutet för varje case så hade de varit inlåsta

/**
 * Sammanfattningsvis fungerar switch satser på liknande sätt som if satser i att den hindrar körandet av kod om inte vissa krav uppfylls
 * dock är den inte lika öppen för förändring som if satser är
 * 
 * man man få samma switch case med break resultat från if satser om man vill med else if checkar
 */